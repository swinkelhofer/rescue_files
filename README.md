# Rescue files in broken image/device

Rescue files on broken filesystems (e.g. destroyed FAT). Via docker run you can give a list of filetypes you want to rescue, or "all" to rescue all known filetypes.

For the moment known filetypes are:
 * PDF
 * PNG
 * JPG
 * ZIP (including all derivates like docx, xlsx, pptx, odt, ods, odp)

## Usage

 * via stdin
```
cat <dev or image_file> | docker run -i --volume <rescued_folder>:/export registry.gitlab.com/swinkelhofer/rescue_files/rescue:latest <[filetype1] [filetypeN] | [all]>
```

 * via volume
```
docker run --volume <rescued_folder>:/export --volume <image_file>:/img/dump -e RESCUE_PATH=/img/dump -it registry.gitlab.com/swinkelhofer/rescue_files/rescue:latest  <[filetype1] [filetypeN] | [all]>
docker run --volume <rescued_folder>:/export --privileged --device /dev/XXX:/dev/XXX -e RESCUE_PATH=/dev/XXX -it registry.gitlab.com/swinkelhofer/rescue_files/rescue:latest  <[filetype1] [filetypeN] | [all]>
```
