#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(){
    char buf[512];
	int magic = 0x464f4525; // %EOF Symbol
    while(read(0, buf, 512) != 0)
    {
        for(int i = 0; i < 512;++i) {
    		if((*((int*)(buf+i))) == (magic) && i < 508) {
            	printf("%c%c%c%c", buf[i], buf[i+1], buf[i+2], buf[i+3]);
				exit(0);
        	}
			printf("%c", buf[i]);
        }
    }
    return 0;
}
