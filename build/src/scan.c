#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

typedef void (*filetype_func)( const char *buf, int offset);
void JPG( const char *buf, int offset ) {
    /* int magic = 0xffd8ffe0; */
    int magic = 0xd8ffd8ff;
    int magic2 = 0x4649464a;
    int magic3 = 0x66697845;
    for(int j = 0; j < 508; ++j) {
        if(*((int*)(buf+j)) == magic) {
            printf("Found JPG at offset %x\n", offset + j);
        }
	if(*((int*)(buf+j)) == magic2 || *((int*)(buf+j)) == magic3) {
	    printf("Found JPG at offset %x\n", offset + j - 6);
	}
    }
}
void PNG( const char *buf, int offset ) {
    int magic = 0x474e5089; //89 50 4E 47;
    for(int j = 0; j < 508; ++j) {
        if((*((int*)(buf+j))) == (magic) ) {
            printf("Found PNG at offset %x\n", offset + j);
        }
    }
}
void PDF( const char *buf, int offset ) {
    int magic = 0x46445025; //89 50 4E 47;
    for(int j = 0; j < 508; ++j) {
        if((*((int*)(buf+j))) == (magic) ) {
            printf("Found PDF at offset %x\n", offset + j);
        }
    }
}
void ZIP( const char *buf, int offset ) {
    int magic = 0x04034b50; //89 50 4E 47;
    int magic1 = 0x06054b50; //89 50 4E 47;
    int magic2 = 0x08074b50; //89 50 4E 47;
    for(int j = 0; j < 508; ++j) {
        if((*((int*)(buf+j))) == (magic) ) {
            printf("Found ZIP at offset %x\n", offset + j);
        }
        else if((*((int*)(buf+j))) == (magic1) ) {
            printf("Found ZIP at offset %x\n", offset + j);
        }
        else if((*((int*)(buf+j))) == (magic2) ) {
            printf("Found ZIP at offset %x\n", offset + j);
        }
    }
}
/* void TIFF( const char *buf, int offset ) { */
/*     int magic = 0x002a4949; //89 50 4E 47; */
/*     for(int j = 0; j < 508; ++j) { */
/*         if((*((int*)(buf+j))) == (magic) ) { */
/*             printf("Found TIFF at offset %x\n", offset + j); */
/*         } */
/*     } */
/* } */
struct _ft {
    filetype_func func;
    char *filetype;
};

struct _ft filetypes[] = {
    {
        JPG,
        "JPG"
    },
    {
        PNG,
        "PNG"
    },
    {
        PDF,
        "PDF"
    },
    {
        ZIP,
        "ZIP"
    },
    /* { */
    /*     TIFF, */
    /*     "TIFF" */
    /* } */
};



void usage(char *prog) {
    printf("Usage:\n%s <filetype_number>\nScan for files with filetype = filetype_number:\n", prog);
    for(int i = 0; i < sizeof(filetypes)/sizeof(filetypes[0]); ++i) {
        printf("\t%d: %s\n", i, filetypes[i].filetype);
    }

}

int main(int argc, char *argv[]) {

    if(argc < 2) {
        usage(argv[0]);
        exit(0);
    }
    printf("Search for filetype: %s\n", filetypes[atoi(argv[1])].filetype);

    int offset = 0;
    char *buf = malloc(512);
    int count;
    while((count = read(0, buf, 512)) != 0) {
        filetypes[atoi(argv[1])].func(buf, offset);
        offset += count;
    }

    return 0;
}


