#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void usage(const char *prog) {
    printf("Usage:\n%s <chunk_size> <offset>\n", prog);
}

int main(int argc, char *argv[]) {
    if(argc < 3) {
        usage(argv[0]);
        exit(1);
    }
	int chunklen, offset;
	if(strncmp(argv[1], "0x", 2) == 0)
		sscanf(argv[1], "%x", &chunklen);
	else
		sscanf(argv[1], "%d", &chunklen);
	if(strncmp(argv[2], "0x", 2) == 0)
		sscanf(argv[2], "%x", &offset);
	else
		sscanf(argv[2], "%d", &offset);
		
    char buf[chunklen];
    lseek(0, offset, SEEK_SET);
	read(0, buf, chunklen);
    for(int i = 0; i < chunklen; ++i)
	    printf("%c", buf[i]);
    return 0;
}
